import React from 'react'; 

import Home from './src/screens/Home'
import ContactUs from './src/screens/ContactUs'
import Application from './src/screens/Application'

import { createAppContainer, createStackNavigator } from 'react-navigation'

const RootStack = createStackNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: () => ({
        title: 'หน้าแรก',
        headerBackTitle: null,
        headerTintColor: '#000000',
      }),
      
    },
    ContactUs: {
      screen: ContactUs,
      navigationOptions: () => ({
        title: 'ติดต่อเรา',
        headerBackTitle: 'กลับ',
        headerTintColor: '#000000',
      }),
    },
    Application: {
      screen: Application,
      navigationOptions: () => ({
        title: 'Application',
        headerBackTitle: 'กลับ',
        headerTintColor: '#000000',
      }),
    },
  },
  {
    initialRouteName: 'Home'
  }
);

const RootStackContainer = createAppContainer(RootStack)

export default function App() {
  return <RootStackContainer />
}


import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

class Application extends React.Component {
    state = {
    }

    render() {
        return (
            <View style={styles.containerWrapper}>
                <Text>{'Application Screen'}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    containerWrapper: {
        padding : 20,
        height: '100%',
        backgroundColor: '#ffffff',
    }
})

export default Application
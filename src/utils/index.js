// import AsyncStorage from '@react-native-async-storage/async-storage';

const HOSTNAME = "staging-api.codexconnect.me";
const PORT = '443'
const PROTOCOL = 'https'

const TOKEN = 'https'

const apiUrl = `${PROTOCOL}://${HOSTNAME}:${PORT}`
const apiFlieUrl = `${PROTOCOL}://${HOSTNAME}:${PORT}`,


function checkHttpStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response.body
  } else {
    var error = new Error(response.statusText)
    error.response = response.body
    throw error
  }
}

function parseJSON(response) {
  console.log('fetch success')
  return response.json()
    .then(function (body) {
      return {
        status: response.status,
        statusText: response.statusText,
        body: body
      }
    })
    .catch(function (e) {
      return response;
    })
}


async function call(serverUrl, method, data) {
  console.log("serverUrl", serverUrl)
  let options = {
    method: method,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      // 'Authorization': 'Bearer ' + await AsyncStorage.getItem('token'),
      // 'Authorization': 'Bearer ' + TOKEN
    },
  }
  if (data) {
    options['body'] = JSON.stringify(data)
  }
  return fetch(serverUrl, options)
    .then(parseJSON)
    .then(checkHttpStatus)
    .catch(error => {
      // No response from the server
      console.log("error at utils.index.js line 21")
      console.log(error)
      if (typeof error.response === 'undefined') {
        error.response = {
          status: 408,
          message: 'Cannot connect to the server'
        }
      }
      throw error
    })
}

export function get(url) {
  const serverUrl = `${apiUrl}${url}`
  return call(serverUrl, 'GET')
}

export function post(url, data) {
  const serverUrl = `${apiUrl}${url}`
  return call(serverUrl, 'POST', data)
}

export function put(url, data) {
  const serverUrl = `${apiUrl}${url}`
  return call(serverUrl, 'PUT', data)
}
export function patch(url, data) {
  const serverUrl = `${apiUrl}${url}`
  return call(serverUrl, 'PATCH', data)
}

export function del(url, data) {
  const serverUrl = `${apiUrl}${url}`
  return call(serverUrl, 'DELETE', data)
}


async function ccall(serverUrl, method, data) {
  console.log("serverUrl", serverUrl)
  return fetch(serverUrl, {
    method: method,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'multipart/form-data',
      // 'Authorization': 'Bearer ' + await AsyncStorage.getItem('token'),
      // 'Authorization': 'Bearer ' + TOKEN
    },
    body: data
  })
    .then(parseJSON)
    .then(checkHttpStatus)
    .catch(error => {
      console.log(error)
      console.log(error.response)

      if (typeof error.response === 'undefined') {
        error.response = {
          status: 408,
          message: 'can not connect server'
        }
      }
      throw error
    })
}


export function ppost(url, data) {
  const serverUrl = `${apiFlieUrl}${url}`
  return ccall(serverUrl, 'POST', data)
}
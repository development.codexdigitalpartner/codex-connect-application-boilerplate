import React from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';

class Home extends React.Component {
    state = {
    }

    render() {
        return (
            <View style={styles.containerWrapper}>
                <View style={styles.buttonWrapper}>
                    <Button
                        style={styles.button}
                        title={'Go to "Contact Us"'}
                        onPress={() => this.props.navigation.navigate('ContactUs')}
                    />
                </View>
                <View style={styles.buttonWrapper}>
                    <Button
                        style={styles.button}
                        title={'Go to "Application"'}
                        onPress={() => this.props.navigation.navigate('Application')}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    containerWrapper: {
        padding: 20,
        height: '100%',
        backgroundColor: '#ffffff',
    },
    buttonWrapper: {
        padding: 10,
    }
})

export default Home